var http = require('http');
const events = require("events");

var server = http.createServer(function (request, response){
    routeAddress(request,response);
});

const myEmitter = new events.EventEmitter();
myEmitter.on('error', err => {
    console.log('ERROR: ${err.message}');
});

function routeAddress(request, response){
    [_, op, a, b] = request.url.split("/");
    var op = op;
    var a = parseInt(a);
    var b = parseInt(b);
    response.writeHead(200, {"Content-Type": "text/html"});
    var isError = true;
    if(op == "add"){
        var responseMsg = a + "+" + b +"="+ (a+b);
        isError = false;
    }else if(op == "sub"){
        var responseMsg = a + "-" + b +"="+ (a-b);
        isError = false;
    }else if(op == "div"){
        var responseMsg = a + "/" + b +"="+ (a/b);
        isError = false;
    }else if(op == "mul"){
        var responseMsg = a + "*" + b +"="+ (a*b);
        isError = false;
    }else if(op != "add" || op != "sub"  || op != "div" || op !="mul") {
        var responseMsg = "Wrong operation input";
    }else if(!isNaN(a) || isNaN(b)){
        var responseMsg = "Wrong parameters format";
    }
    
    response.end(responseMsg)	
}
server.listen(8080, "127.0.0.1");
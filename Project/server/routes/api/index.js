const router = require('express').Router();

router.use('/entries', require('./entries'));

module.exports = router;
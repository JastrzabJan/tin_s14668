export default (state={entries: []}, action) => {
    switch(action.type) {
      case 'HOME_PAGE_LOADED':
        return {
          ...state,
          entries: action.data.entries,
        };
      case 'SUBMIT_ENTRY':
        return {
          ...state,
          entries: ([action.data.entry]).concat(state.entries),
        };
      case 'DELETE_ENTRY':
        return {
          ...state,
          entries: state.entries.filter((entry) => entry._id !== action.id),
        };
      default:
        return state;
    }
  };
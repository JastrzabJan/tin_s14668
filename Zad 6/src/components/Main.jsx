import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Form from './Form';
import Values from './Values';
import Label from './Label';

function Main() {
  return (
    <main>
      <Switch>
        <Route exact path='/' component={Form}/>
        <Route path='/values' component={Values}/>
        <Route path='/label' component={Label}/>
      </Switch>
    </main>
  );
}

export default Main;

import React from 'react';
import PropTypes from 'prop-types';

const Label = ({parameter}) => {
    const displayMessage = (parameters) => {
        if (parameters) {
            if(parameters.length > 0) {
                return parameters.map((item, key) => {
                    console.log(key + item);
                    return (
                        <div key={key}>
                            <p>{item}</p>
                        </div>
                    );
                })
            }
        } else {
            return (<p>No parameters.</p>);
        }
    };

    return (
        <div className="body">
            <h2>Validation parameters: </h2>
            {displayMessage(parameter)}
        </div>
    )
};

Label.propTypes = {
    parameter: PropTypes.array,
};

export default Label;

import React from 'react';
import PropTypes from 'prop-types';

const Values = ({
    name,
    birthYear,
    email,
}) => {
    return (
        <div className="body">
            <h2>Values</h2>
            <div className="values">
                <p>Name: {name === undefined ? 'empty' : name}</p>
                <p>Year of Birth: {birthYear === undefined ? 'empty' : birthYear}</p>
                <p>Email: {email === undefined ? 'empty' : email}</p>
            </div>
        </div>
    )
};

Values.propTypes = {
    name: PropTypes.string,
    name: PropTypes.number,
    email: PropTypes.string,
};

export default Values;

import React from 'react';
import { Link } from 'react-router-dom'

function Navigation() {
  return (
    <header className="body">
      <nav>
        <span>Navigation:</span>
        <div className="navList">
          <span><Link to='/'>Form</Link></span>
          <span><Link to='/values'>Values</Link></span>
          <span><Link to='/label'>Label</Link></span>
        </div>
      </nav>
    </header>
  );
}

export default Navigation
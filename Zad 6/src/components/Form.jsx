import React from 'react';
import Label from './Label';
import Values from './Values';

class FormComp extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            birthYear: '',
            email: '',
            parameters: [],
            renderComp: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.validateClick = this.validateClick.bind(this);
    }



    handleChange = (event) => {
        this.setState(
            {
                [event.target.name] : event.target.value
            }
        );
    }

    validateClick = () => {
        console.log('validateClick clicked!');
        let newTab = [];
        if (this.state.name === '' && this.state.birthYear === '' && this.state.email === '') {
            newTab.push('Nothing to validate.');
            this.setState(
                {
                    parameters : newTab,
                    renderComp : true,
                }
            ); 
        } else {
            newTab = [];
            if (this.state.name) {
                if (this.state.name.length < 4) {
                    newTab.push('Name must have at least 4 letters');
                    this.setState(
                        {
                            parameters : newTab,
                            renderComp : true,
                        }
                    ); 
                }
            }
            if (this.state.birthYear) {
                if (this.state.birthYear <= 1900 ) {
                    newTab.push('Year of birth must higher than 1930');
                }
                let currentYear = new Date().getFullYear();
                if (this.state.birthYear > currentYear ) {
                    newTab.push('Birth year cannot be higher than current date');
                }; 
            }
            if (this.state.email) {
                if (this.state.email.indexOf("@") === -1) {
                    newTab.push('Email has to contain @');
                }
            }
            if (newTab.length === 0) {
                newTab.push('values are valid');
            }
            this.setState(
                {
                    parameters : newTab,
                    renderComp : true,
                }
            );
        }

    }

    render() {

        return (
            <div>
                <div className="topDisplay">
                    <form onSubmit={()=> {console.log('submitting...')}} className="body">
                        <h2>Form</h2>
                        <div className="formList">
                            <div className="formItem">
                                <span>Name: </span>
                                <input
                                    type="text"
                                    id="name"
                                    name="name"
                                    required
                                    minlength="4"
                                    maxlength="15"
                                    size="20"
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="formItem">
                                <span>Year of Birth: </span>
                                <input
                                    type="number"
                                    id="birthYear"
                                    name="birthYear"
                                    required
                                    maxlength="4"
                                    value={this.state.birthYear}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="formItem">
                                <span>Email: </span>
                                <input
                                    type="email"
                                    id="email"
                                    name="email"
                                    required
                                    minlength="4"
                                    maxlength="15"
                                    size="20"
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="buttons">   
                            <input
                                className="submitButton"
                                type="button"
                                value="Validate"
                                onClick={() => this.validateClick()}
                            />
                        </div>
                    </form>
                    <Values 
                        name={this.state.name}
                        birthYear={this.state.birthYear}
                        email={this.state.email}
                    />
                </div>
                {this.state.renderComp ? <Label 
                        parameter={this.state.parameters}
                    /> : null 
                }
            </div>
        );
    }
}

export default FormComp;
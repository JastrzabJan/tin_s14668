import React, { Component } from 'react';
import './styles/App.css';
import Main from './components/Main';
import Navigation from './components/Navigation';
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Navigation />
          <Main />
        </header>
      </div>
    );
  }
}

export default App;

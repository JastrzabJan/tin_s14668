function findLongestWord(array) {
    var splittedArray = array.split(",");
    var wordLength = 0;
    var result = null;
    for (var i = 0; i < splittedArray.length; i++) {
        if (wordLength < splittedArray[i].length) {
            wordLength = splittedArray[i].length;
            result = splittedArray[i];
        }
    }
    return result;
}


var array = 'Testing,Javascript,LongestWordHere,Checking';
console.log(findLongestWord(array));
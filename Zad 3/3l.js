function Student(name, surname, index, grades) {
    this.name = name;
    this.surname = surname;
    this.index = index;
    this.grades = grades;

    this.average = function(){
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += this.grades[i];
        }
        return sum / this.grades.length;
    };

   this.info = function() {
		console.log("Name :" + this.name);
		console.log("Surname :" + this.surname);
		console.log("Average :" + this.average());
	};
}

var newStudent = new Student('Jan', 'Kowalski', '15665', [3,3,5,5,2]);
newStudent.info();

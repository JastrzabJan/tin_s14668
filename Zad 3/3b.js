function fibonacci_count(number){
	if (number <= 1) return 1;

  return fibonacci_count(number - 1) + fibonacci_count(number - 2);
}

console.log(fibonacci_count(16));
function returnType(object){
		return typeof(object);
}

var computer = {
	graphics: 8,
	ram: 16,
	type: 'gaming',
	changeRam(newRam){
		this.ram = newRam;
	},
	changeType(newType){
		this.type = newType;
	}
};

function showComputer(ParamComputer){
	console.log(ParamComputer.graphics + ", type of graphics: " + returnType(ParamComputer.graphics));
	console.log(ParamComputer.ram + ", type of ram:  " + returnType(ParamComputer.ram));
	console.log(ParamComputer.type + ", type of type:  " + returnType(ParamComputer.type));
}
showComputer(computer);
function checkPrimeNumber(number) {
  for(var i = 2; i < number; i++) {
    if(number % i === 0) return false;
  }
  return number !== 1 && number !== 0;
}

console.log(checkPrimeNumber(7));
console.log(checkPrimeNumber(4));
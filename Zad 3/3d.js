function sort_alphabetically(word) {
  var arrayStr = word.split('');
  var sorted = arrayStr.sort();
  return sorted.join('');
}

var toCheck = 'testing';
console.log(sort_alphabetically(toCheck));
var toCheck = 'second';
console.log(sort_alphabetically(toCheck));
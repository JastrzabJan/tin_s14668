

var CelsiusToFahrenheits = function(value) {
	return value * 9 / 5 + 32;
}

var FahrenheitsToCelsius = function(value) {
	return (value - 32) * 5 / 9;
}

function submit() {
    var valuCelcius = document.getElementById('valueCelcius').value;
    var valueFahrenheits = document.getElementById("valueFahrenheits").value;
    document.getElementById('cels').textContent = FahrenheitsToCelsius(valueFahrenheits);
    document.getElementById('fahren').textContent = CelsiusToFahrenheits(valuCelcius);
}
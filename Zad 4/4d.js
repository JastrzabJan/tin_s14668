

function validate() {
  var name, surname, age, email, name_msg, email_msg, email_format_msg, age_msg;

  name = document.getElementById("name").value;
  surname = document.getElementById("surname").value;
  age = document.getElementById("age").value;
  email = document.getElementById("email").value;

  name_msg="Name is required";
  age_msg="Age is required";
  email_msg="Email is required";
  email_format_msg="Email has wrong format"; 
  
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  is_valid= true;


  if(name === ""){
    document.getElementById("name_msg").innerHTML=name_msg;
    is_valid = false;
  }else{
    document.getElementById("name_msg").innerHTML="";
  }

  if(age === ""){
    document.getElementById("age_msg").innerHTML=age_msg;
    is_valid=false;
  }else{
    document.getElementById("age_msg").innerHTML="";
  }

  if(email === ""){
    document.getElementById("email_msg").innerHTML=email_msg;
    is_valid = false;
  }else if(!re.test(email)){
    document.getElementById("email_msg").innerHTML="";
    document.getElementById("email_format_msg").innerHTML = email_format_msg;
    is_valid = false;
  }else{
    document.getElementById("email_msg").innerHTML="";
    document.getElementById("email_format_msg").innerHTML = "";
  }


  console.log(is_valid);
  return is_valid;
}